/*
 * Decompiled with CFR 0.150.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 *  com.google.gson.reflect.TypeToken
 *  com.massivecraft.factions.Board
 *  com.massivecraft.factions.FLocation
 *  org.bukkit.Chunk
 *  org.bukkit.Location
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.world.ChunkLoadEvent
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.java.JavaPlugin
 */
package ninja.mcknight.chuckrefresher;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class ChunkRefresherPlugin
extends JavaPlugin
implements Listener,
Runnable {
    private Gson gson;
    private Map<String, Long> chunks;

    public void onEnable() {
        this.gson = new GsonBuilder().create();
        this.load();
        this.getServer().getPluginManager().registerEvents((Listener)this, (Plugin)this);
        this.getServer().getScheduler().runTaskTimerAsynchronously((Plugin)this, (Runnable)this, 1200L, 1200L);
    }

    public void onDisable() {
        this.save();
        this.chunks = null;
        this.gson = null;
    }

    private void load() {
        File chunksFile = new File(this.getDataFolder(), "chunks.json");
        if (!chunksFile.exists()) {
            this.chunks = new ConcurrentHashMap<String, Long>();
            return;
        }
        try {
            FileReader reader = new FileReader(chunksFile);
            Type t = new TypeToken<Map<String, Long>>(){}.getType();
            Map localChunks = (Map)this.gson.fromJson((Reader)reader, t);
            this.chunks = new ConcurrentHashMap<String, Long>(localChunks);
        }
        catch (FileNotFoundException fileNotFoundException) {
            // empty catch block
        }
    }

    private void save() {
        HashMap<String, Long> localChunks = new HashMap<String, Long>(this.chunks);
        File chunksFile = new File(this.getDataFolder(), "chunks.json");
        try {
            FileWriter writer = new FileWriter(chunksFile);
            this.gson.toJson(localChunks, (Appendable)writer);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled=true)
    public void onPlayerMove(PlayerMoveEvent event) {
        int tz;
        int tx;
        Location f = event.getFrom();
        Location t = event.getTo();
        int fx = f.getBlockX() >> 4;
        if (fx != (tx = t.getBlockX() >> 4)) {
            return;
        }
        int fz = f.getBlockZ() >> 4;
        if (fz != (tz = t.getBlockZ() >> 4)) {
            return;
        }
        FLocation ff = new FLocation(f);
        FLocation ft = new FLocation(t);
        if (Board.getFactionAt((FLocation)ff).isWarZone()) {
            return;
        }
        if (Board.getFactionAt((FLocation)ft).isWarZone()) {
            return;
        }
        this.chunks.put(tx + "_" + tz, System.currentTimeMillis());
    }

    @EventHandler(priority=EventPriority.MONITOR, ignoreCancelled=true)
    public void onChunkLoad(ChunkLoadEvent event) {
        Chunk c = event.getChunk();
        Location loc = new Location(c.getWorld(), (double)FLocation.chunkToBlock((int)c.getX()), 80.0, (double)FLocation.chunkToBlock((int)c.getZ()));
        FLocation floc = new FLocation(loc);
        if (!Board.getIdAt((FLocation)floc).equals("0")) {
            return;
        }
        String key = this.getKey(c.getX(), c.getZ());
        Long time = this.chunks.get(key);
        if (time == null) {
            this.chunks.put(key, System.currentTimeMillis());
            return;
        }
        long timedays = (System.currentTimeMillis() - time) / 1000L / 3600L / 24L;
        if (timedays > 14L) {
            c.getWorld().regenerateChunk(c.getX(), c.getZ());
        }
    }

    public String getKey(int x, int z) {
        return x + "_" + z;
    }

    @Override
    public void run() {
        this.save();
    }
}

